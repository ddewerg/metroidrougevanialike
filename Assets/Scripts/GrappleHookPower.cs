﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHookPower : Power {
    Rigidbody2D rb;
    HingeJoint2D hinge;
    public float force = 10;
    public int ropeSegments = 5;
    public float ropeLength = 5;
    public HingeJoint2D ropeSegment;
    HingeJoint2D[] ropeSegInstances;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        FixedJoint2D fj =  gameObject.AddComponent<FixedJoint2D>();
        fj.connectedBody = col.rigidbody;
    }


    public override void StartPower()
    {
        base.StartPower();
        rb = GetComponent<Rigidbody2D>();
        hinge = GetComponent<HingeJoint2D>();
        ropeSegInstances = new HingeJoint2D[ropeSegments];
        for (int i = ropeSegments -1; i >= 0; i--)
        {
            ropeSegInstances[i] = Instantiate(ropeSegment);
            ropeSegInstances[i].transform.position = handTransform.position;
            ropeSegInstances[i].transform.localScale = new Vector3(ropeLength / ropeSegments, ropeSegment.transform.localScale.y, ropeSegment.transform.localScale.z);
            if (i < ropeSegments - 1)
            {
                ropeSegInstances[i].connectedBody = ropeSegInstances[i + 1].GetComponent<Rigidbody2D>();
            }
            else
            {
                ropeSegInstances[i].connectedBody = player.GetComponent<Rigidbody2D>();
            }
        }
        hinge.connectedBody = ropeSegInstances[0].GetComponent<Rigidbody2D>();
        rb.AddForce((aimpoint - new Vector2(transform.position.x, transform.position.y)) * force, ForceMode2D.Impulse);
    }

    public override void UpdatePower()
    {
        
    }

    public override void ReleasePower()
    {
        base.ReleasePower();
        for (int i = 0; i < ropeSegInstances.Length; i++)
            Destroy(ropeSegInstances[i].gameObject);
    }
}
