﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityHud : MonoBehaviour {
    public RangedArm rangedArm;
    public Image[] abilityImages;
    public Image[] cooldownSprites;
    public Image highlightImage;
	// Update is called once per frame
	void Update () {
        highlightImage.rectTransform.position = abilityImages[rangedArm.selectedPower].rectTransform.position;
        for(int i = 0; i < abilityImages.Length; i++)
        {
            if (i >= rangedArm.powers.Count)
            {
                abilityImages[i].color = Color.clear;
                cooldownSprites[i].fillAmount = 0;
            }
            else
            {
                cooldownSprites[i].fillAmount = rangedArm.powers[i].remainingCooldown / rangedArm.powers[i].cooldown;
                abilityImages[i].sprite = rangedArm.powers[i].powerIcon;
                abilityImages[i].color = Color.white;
            }
        }

	}
}
