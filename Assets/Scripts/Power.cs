﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Power : MonoBehaviour {
    public GameObject player;
    public Vector2 aimpoint;
    public Vector2 endAimpoint;
    public Transform handTransform;
    public float baseSlowTimeFactor = 0.5f;
    public TimeControl tc;

    public virtual void StartPower()
    {
        tc.SlowTime(baseSlowTimeFactor * (1-FlowManager.Flow));
    }

    public virtual void UpdatePower()
    {

    }

    public  virtual void ReleasePower()
    {
        tc.ResumeTime();
        Destroy(gameObject);
    }
}
