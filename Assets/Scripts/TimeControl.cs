﻿using System;
using System.Collections;
using UnityEngine;


public class TimeControl : MonoBehaviour
{
    public float timeChangeInterval = 0.2f;
    

    public void SlowTime(float timeMul)
    {
        StartCoroutine(SlowTimeCo(timeMul));
    }

    public void ResumeTime()
    {
        StartCoroutine(ResumeTimeCo());
    }

    IEnumerator SlowTimeCo(float timeMul)
    {
        float time = 0;
        while (time < timeChangeInterval)
        {
            yield return null;
            time += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(1, timeMul, time / timeChangeInterval);
        }
    }

    IEnumerator ResumeTimeCo()
    {
        float time = 0;
        float curTimeScale = Time.timeScale;
        while (time < timeChangeInterval)
        {
            yield return null;
            time += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(curTimeScale, 1, time / timeChangeInterval);
        }
    }
}
