﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedArm : MonoBehaviour {
    public Transform IKNode;
    public Transform handNode;
    public Vector2 aimPoint;
    public Vector2 shoulderOffset;
    public float armLength = 1;
    public List<RangedAbility> powers;
    GameObject handObject;
    public int selectedPower
    {
        get
        {
            return _selectedPower;
        }
        set
        {
            if (value < 0)
                selectedPower = powers.Count + value;
            else if (value >= powers.Count)
                selectedPower = value - powers.Count;
            else
            {
                _selectedPower = value;
                if (handObject != null)
                    Destroy(handObject);
                if(powers[_selectedPower].handObject != null)
                {
                    handObject = Instantiate(powers[_selectedPower].handObject);
                    handObject.transform.SetParent(handNode);
                    handObject.transform.localPosition = Vector3.zero;
                }
            }
        }
    }
    int _selectedPower = 0;

	// Update is called once per frame
	void Update () {
        Vector2 aimVec = (aimPoint - (new Vector2(transform.position.x, transform.position.y) + shoulderOffset));
        IKNode.transform.position = transform.position + new Vector3(shoulderOffset.x, shoulderOffset.y) + new Vector3(aimVec.x, aimVec.y).normalized * Mathf.Min(armLength, aimVec.magnitude);
        IKNode.transform.rotation = Quaternion.FromToRotation(new Vector3(1, 0, 0), new Vector3(aimVec.x, aimVec.y, 0).normalized);
        foreach (var power in powers)
        {
            power.UpdateCooldown();
        }
	}
}
