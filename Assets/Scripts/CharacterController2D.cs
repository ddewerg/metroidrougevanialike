﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController2D : MonoBehaviour {

    Rigidbody2D rb;
    public bool grounded = false;
    public Vector2 footOffset;
    public float jumpForce = 10;
    public float runSpeed = 5;
    public float airControl = 0.7f;
    public float accelerationFactor = 10.0f;
    public Animator anim;
    int direction = 1;
    public Camera cam;
    Vector2 aimPoint = Vector2.zero;
    RangedArm r;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        r = GetComponent<RangedArm>();		
	}
	
	// Update is called once per frame
	void Update () {
        aimPoint = cam.ScreenToWorldPoint(Input.mousePosition);
        r.aimPoint = aimPoint;
        direction = (int)Mathf.Sign(aimPoint.x - transform.position.x);
        anim.transform.rotation = Quaternion.Euler(new Vector3(0, 90 * direction, 0));
        RaycastHit2D rc = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y) + footOffset, new Vector2(0, -1), 0.05f, 1 << LayerMask.NameToLayer("Terrain"));
        grounded = rc;
        rb.angularVelocity = 0;
        rb.rotation = 0;
        if (rb.isKinematic)
        {
            rb.velocity = new Vector2(rb.velocity.x,Mathf.Max(0, rb.velocity.y));
        }
        if(grounded && Input.GetButtonDown("Jump"))
        {
            rb.velocity += new Vector2(0, 1) * jumpForce;
        }
        if (r.powers.Count > 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                r.powers[r.selectedPower].Fire(gameObject, aimPoint, r.IKNode);
            }
            else if (Input.GetButton("Fire1"))
            {
                r.powers[r.selectedPower].UpdateObject(aimPoint);
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                r.powers[r.selectedPower].ReleaseObject(aimPoint);
            }
        }
        if (Input.GetButtonDown("NextPower"))
        {
            r.selectedPower++;
        }
        if (Input.GetButtonDown("PrevPower"))
        {
            r.selectedPower--;
        }
        rb.velocity = Vector2.LerpUnclamped(rb.velocity, new Vector2(Input.GetAxis("Horizontal") * runSpeed, Mathf.Clamp(rb.velocity.y, -100, 0)), accelerationFactor * (grounded ? 1 : airControl) * Time.deltaTime);
        anim.SetFloat("Speed", Mathf.Sign(direction) * rb.velocity.x / runSpeed);
        anim.SetFloat("FallSpeed", Mathf.Abs(rb.velocity.y));
        anim.SetBool("Grounded", grounded);
	}
}
