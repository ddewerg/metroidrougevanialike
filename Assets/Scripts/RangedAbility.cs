﻿using System;
using System.Collections;
using UnityEngine;
[CreateAssetMenu(fileName = "RangedPower", menuName = "Powers/Ranged")]
public class RangedAbility : ScriptableObject
{
    public Power power;
    public GameObject handObject;
    public float cooldown = 1;
    [System.NonSerialized]
    public float remainingCooldown = 0;
    Power pow;
    bool powerActive = false;
    public Sprite powerIcon;

    public void Fire(GameObject player, Vector2 aimPoint, Transform handTransform)
    {
        if (remainingCooldown <= 0)
        {
            pow = Instantiate(power);
            pow.aimpoint = pow.endAimpoint = aimPoint;
            pow.player = player;
            pow.handTransform = handTransform;
            pow.tc = FindObjectOfType<TimeControl>();
            pow.transform.position = handTransform.position;
            pow.transform.right = handTransform.right;
            pow.StartPower();
            powerActive = true;
        }
    }

    public void UpdateObject(Vector2 aimPoint)
    {
        if (powerActive)
        {
            pow.endAimpoint = aimPoint;
            pow.UpdatePower();
        }
    }

    public void ReleaseObject(Vector2 aimPoint)
    {
        if (powerActive)
        {
            pow.endAimpoint = aimPoint;
            pow.ReleasePower();
            remainingCooldown = cooldown;
            powerActive = false;
        }
    }

    public void UpdateCooldown()
    {
        remainingCooldown = Mathf.Max(0, remainingCooldown - Time.deltaTime);
    }

}
