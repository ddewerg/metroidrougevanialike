﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : Power {
    public Projectile projectilePrefab;

    public override void ReleasePower()
    {
        Projectile p = Instantiate(projectilePrefab);
        p.transform.position = handTransform.position;
        p.transform.right = handTransform.right;
        base.ReleasePower();
    }
}
