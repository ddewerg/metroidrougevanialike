﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashPower : Power
{
    public float dashVelocity = 10;

    public override void ReleasePower()
    {
        Vector2 dashDir = (endAimpoint - (Vector2)player.transform.position).normalized;
        Rigidbody2D r = player.GetComponent<Rigidbody2D>();
        r.velocity = dashDir * dashVelocity;
        base.ReleasePower();
    }
}
