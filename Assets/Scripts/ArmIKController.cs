﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmIKController : MonoBehaviour {
    public Transform leftIK;
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftIK.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);   
    }
}
