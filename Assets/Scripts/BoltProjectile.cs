﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltProjectile : BulletProjectile {
    bool embedded = false;

    protected override void Update()
    {
        if(!embedded)
        {
            transform.right = rb.velocity.normalized;
        }
    }

    protected override void OnCollisionEnter2D(Collision2D col)
    {
        if (!embedded)
        {
            FixedJoint2D fj = gameObject.AddComponent<FixedJoint2D>();
            fj.connectedBody = col.rigidbody;
            fj.enableCollision = false;
            gameObject.layer = LayerMask.NameToLayer("Item");
            embedded = true;
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
