﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BulletProjectile : Projectile {

    public float speed = 10;
    public float colliderEnableDelay = 0.05f;
    protected Rigidbody2D rb;
    protected Collider2D c;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        c = GetComponent<Collider2D>();
        c.enabled = false;
        StartCoroutine(enableCollider());
	}
    protected virtual void Update()
    {
        rb.transform.right = rb.velocity.normalized;
    }

    virtual protected void OnCollisionEnter2D(Collision2D col)
    {
        Destroy(gameObject);
    }

    IEnumerator enableCollider()
    {
        yield return new WaitForSeconds(colliderEnableDelay);
        c.enabled = true;
    }
}
