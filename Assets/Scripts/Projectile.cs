﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    public GameObject player;
    public Vector2 aimpoint;
    public Vector2 endAimpoint;
}
